<?php

namespace App\Http\Controllers;

use App\Image;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Log;
use Validator;
class HomeController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$data['pages'] = Page::all();

		return view('home', $data);
	}

	public function pageEditor()
	{
		return view('page-editor');
	}

	public function imageUpload(Request $request){


		$path = $request->file('files')[0]->store('public/uploads');
		$path = explode('/',$path)[2];
		Image::create(['url'=>$path]);
		$data['data'] = array();
		$temp = [
			'src' => $path
		];
		array_push($data['data'],$temp);
		return json_encode($data);
	}
}
