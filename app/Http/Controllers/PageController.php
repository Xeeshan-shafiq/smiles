<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;
use App\Page;
use Illuminate\Support\Facades\Redirect;

class PageController extends Controller
{
	public function create()
	{
		return view('page.create-page');
	}

	public function store(Request $request)
	{
		$data = $request->all();

		$page = new Page();
		$page->title = $data['title'];
		$page->slug = $data['slug'];
		$page->save();


		return Redirect::to('/home');
	}


	public function editSinglePage($slug){
		$data['page'] = Page::where('slug',$slug)->first();
		$data['images'] = Image::all();
		return view('page-editor',$data);
	}

	public function savePage(Request $request)
	{
		$html = $request->description;
		$page = Page::where('slug',$request->slug)->first();
		$page->description = $html;
		$page->css = $request->css;
		if ($page->save()) {
			return response(['error' => 0], 200);
		} else {
			return response(['error' => 1], 400);
		}
	}

	public function page($slug)
	{
		$page = Page::where('slug',$slug)->first();

		return view('page',compact('page',$page));
	}
}
