/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
    el: '#app'
});


var blkStyle = '.blk-row::after{ content: ""; clear: both; display: block;} .blk-row{padding: 10px;}';
var editor = grapesjs.init({
    allowScripts: 1,
    showOffsets: 1,
    autorender: 0,
    noticeOnUnload: 0,
    container: '#gjs',
    height: '700px',
    fromElement: true,
    clearOnRender: 0,
    storageManager: {
        autoload: 0
    },
    cssComposer:{
        rules: css
    },
    commands: {
        defaults: [{
            id: 'open-github',
            run: function (editor, sender) {
                sender.set('active', false);
                window.open('https://github.com/artf/grapesjs', '_blank');
            }
        }, {
            id: 'undo',
            run: function (editor, sender) {
                sender.set('active', false);
                editor.UndoManager.undo(true);
            }
        }, {
            id: 'redo',
            run: function (editor, sender) {
                sender.set('active', false);
                editor.UndoManager.redo(true);
            }
        }, {
            id: 'clean-all',
            run: function (editor, sender) {
                sender.set('active', false);
                if (confirm('Are you sure to clean the canvas?')) {
                    var comps = editor.DomComponents.clear();
                }
            }
        }]
    },
    assetManager: {
        upload: '/image-upload',
        params: {
            _token: 'pCYrSwjuiV0t5NVtZpQDY41Gn5lNUwo3it1FIkAj'
        },
        assets: files
    },

    blockManager: {
        blocks: [{
            id: 'b1',
            label: '1 Block',
            category: 'Basic',
            attributes: {class: 'gjs-fonts gjs-f-b1'},
            content: `<div class="row" data-gjs-droppable=".cell" data-gjs-custom-name="Row">
                <div class="col-md-12" data-gjs-draggable=".row" data-gjs-custom-name="Cell"><p>Place your items here</p></div>
              </div>`
        }, {
            id: 'b2',
            label: '2 Blocks',
            category: 'Basic',
            attributes: {class: 'gjs-fonts gjs-f-b2'},
            content: `<div class="row" data-gjs-droppable=".cell" data-gjs-custom-name="Row">
                <div class="col-md-6" data-gjs-draggable=".row" data-gjs-custom-name="Cell"><p>Place your items here</p></div>
                <div class="col-md-6" data-gjs-draggable=".row" data-gjs-custom-name="Cell"><p>Place your items here</p></div>
              </div>`
        }, {
            id: 'b3',
            label: '3 Blocks',
            category: 'Basic',
            attributes: {class: 'gjs-fonts gjs-f-b3'},
            content: `<div class="row" data-gjs-droppable=".cell" data-gjs-custom-name="Row">
                <div class="col-md-4" data-gjs-draggable=".row" data-gjs-custom-name="Cell"><p>Place your items here</p></div>
                <div class="col-md-4" data-gjs-draggable=".row" data-gjs-custom-name="Cell"><p>Place your items here</p></div>
                <div class="col-md-4" data-gjs-draggable=".row" data-gjs-custom-name="Cell"><p>Place your items here</p></div>
              </div>`
        }, {
            id: 'b4',
            label: '3/7 Block',
            category: 'Basic',
            attributes: {class: 'gjs-fonts gjs-f-b37'},
            content: `<div class="row" data-gjs-droppable=".cell" data-gjs-custom-name="Row">
                <div class="col-md-4" data-gjs-draggable=".row" data-gjs-custom-name="Cell"><p>Place your items here</p></div>
                <div class="cell cellcol-md-8" data-gjs-draggable=".row" data-gjs-custom-name="Cell"><p>Place your items here</p></div>
              </div>`,
        }, {
            id: 'hero',
            label: 'Hero section',
            category: 'Section',
            content: '<header class="header-banner"> <div class="container-width">' +
            '<div class="logo-container"><div class="logo">GrapesJS</div></div>' +
            '<nav class="navbar">' +
            '<div class="menu-item">BUILDER</div><div class="menu-item">TEMPLATE</div><div class="menu-item">WEB</div>' +
            '</nav><div class="clearfix"></div>' +
            '<div class="lead-title">Build your templates without coding</div>' +
            '<div class="lead-btn">Try it now</div></div></header>',
            attributes: {class: 'gjs-fonts gjs-f-hero'}
        }, {
            id: 'h1p',
            label: 'Text section',
            category: 'Typography',
            content: `<div>
              <h1 class="heading">Insert title here</h1>
              <p class="paragraph">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
              </div>`,
            attributes: {class: 'gjs-fonts gjs-f-h1p'}
        }, {
            id: '3ba',
            label: 'Badges',
            category: 'Section',
            content: '<div class="badges">' +
            '<div class="badge">' +
            '<div class="badge-header"></div>' +
            '<img class="badge-avatar" src="img/team1.jpg">' +
            '<div class="badge-body">' +
            '<div class="badge-name">Adam Smith</div><div class="badge-role">CEO</div><div class="badge-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore ipsum dolor sit</div>' +
            '</div>' +
            '<div class="badge-foot"><span class="badge-link">f</span><span class="badge-link">t</span><span class="badge-link">ln</span></div>' +
            '</div>' +
            '<div class="badge">' +
            '<div class="badge-header"></div>' +
            '<img class="badge-avatar" src="img/team2.jpg">' +
            '<div class="badge-body">' +
            '<div class="badge-name">John Black</div><div class="badge-role">Software Engineer</div><div class="badge-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore ipsum dolor sit</div>' +
            '</div>' +
            '<div class="badge-foot"><span class="badge-link">f</span><span class="badge-link">t</span><span class="badge-link">ln</span></div>' +
            '</div>' +
            '<div class="badge">' +
            '<div class="badge-header"></div>' +
            '<img class="badge-avatar" src="img/team3.jpg">' +
            '<div class="badge-body">' +
            '<div class="badge-name">Jessica White</div><div class="badge-role">Web Designer</div><div class="badge-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore ipsum dolor sit</div>' +
            '</div>' +
            '<div class="badge-foot"><span class="badge-link">f</span><span class="badge-link">t</span><span class="badge-link">ln</span>' +
            '</div>' +
            '</div></div>',
            attributes: {class: 'gjs-fonts gjs-f-3ba'}
        }, {
            id: 'text',
            label: 'Text',
            attributes: {class: 'gjs-fonts gjs-f-text'},
            category: 'Basic',
            content: {
                type: 'text',
                content: 'Insert your text here',
                style: {padding: '10px'},
                activeOnRender: 1
            },
        }, {
            id: 'image',
            label: 'Image',
            category: 'Basic',
            attributes: {class: 'gjs-fonts gjs-f-image'},
            content: {
                style: {color: 'black'},
                type: 'image',
                activeOnRender: 1
            },
        }, {
            id: 'quo',
            label: 'Quote',
            category: 'Typography',
            content: '<blockquote class="quote">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore ipsum dolor sit</blockquote>',
            attributes: {class: 'fa fa-quote-right'}
        }, {
            id: 'link',
            label: 'Link',
            category: 'Basic',
            attributes: {class: 'fa fa-link'},
            content: {
                type: 'link',
                content: 'Link',
                style: {color: '#d983a6'}
            },
        }, {
            id: 'map',
            label: 'Map',
            category: 'Extra',
            attributes: {class: 'fa fa-map-o'},
            content: {
                type: 'map',
                style: {height: '350px'}
            },
        }, {
            id: 'video',
            label: 'Video',
            category: 'Basic',
            attributes: {class: 'fa fa-youtube-play'},
            content: {
                type: 'video',
                src: 'img/video2.webm',
                style: {
                    height: '350px',
                    width: '615px',
                }
            },
        },
            {
                id: 'call_to_action',
                label: 'Call to Action',
                category: 'Call To Actions',
                attributes: {class: 'fa fa-hand-pointer-o'},
                content: '<section class="s-call-to-action _lg _radial"> <div style="background-color: #554ad7; background-image: radial-gradient(ellipse at center, #9793e7, #554ad7 500px)" class="s-call-to-action__overlay"></div> <div class="container"> <div class="s-call-to-action__wrapper"> <h2 class="s-call-to-action__title _medium _title _lighter">Looking to save on [treatment name]?</h2> <h3 class="s-call-to-action__text _lighter">Whether you need a check-up, cosmetic dentistry, dentures or root canal, all included in smile.com.au dental cover. Join for under $100 per year to save 15% to 40% on all dental fees at quality approved dentists.</h3> <div class="s-call-to-action__actions"> <a href="#a" class="btn _lg _primary _join"> <div class="btn__wrapper"> <div class="btn__text">join smile.com.au &amp; save</div> <div class="btn__progress"> <div class="btn__progress-icon"> <svg focusable="false" version="1.1" width="0" height="0" class="svg-progress"> <use xlink:href="#svg-progress"></use> </svg> </div> </div> </div> </a> </div> </div> </div> </section>',
            },
            {
                id: 'call_to_action_faq',
                label: 'Call to Action FAQ',
                category: 'Call To Actions',
                attributes: {class: 'fa fa-hand-pointer-o'},
                content: '<section class="s-call-to-action _lg _radial"> <div style="background-color: #554ad7; background-image: radial-gradient(ellipse at center, #9793e7, #554ad7 500px)" class="s-call-to-action__overlay"></div> <div class="container"> <div class="s-call-to-action__wrapper"> <h2 class="s-call-to-action__title _medium _title _lighter">Put your smile first</h2> <h3 class="s-call-to-action__text _lighter">We\'re creating a new era in dental cover so everyone can afford a smile than keeps them healthy and makes them proud. It\'s easy: join i two minutes, find your local smile.com.au approved dentist and celebrate because you saved money plus you have a beutiful, healthy smile.</h3> <div class="s-call-to-action__actions"> <button type="button" class="btn _lighter _bordered _primary _lg _inverted"> <div class="btn__wrapper"> <div class="btn__text">find an approved dentist</div> <div class="btn__progress"> <div class="btn__progress-icon"> <svg focusable="false" version="1.1" width="0" height="0" class="svg-progress"> <use xlink:href="#svg-progress"></use> </svg> </div> </div> </div> </button> <button type="button" class="btn _primary _lg"> <div class="btn__wrapper"> <div class="btn__text">join smile.com.au</div> <div class="btn__progress"> <div class="btn__progress-icon"> <svg focusable="false" version="1.1" width="0" height="0" class="svg-progress"> <use xlink:href="#svg-progress"></use> </svg> </div> </div> </div> </button> </div> </div> </div> </section>',
            },
            {
                id: 'call_to_action_faq',
                label: 'Call to Action Savings',
                category: 'Call To Actions',
                attributes: {class: 'fa fa-hand-pointer-o'},
                content: '<section class="s-call-to-action _lg _radial"> <div style="background-color: #554ad7; background-image: radial-gradient(ellipse at center, #9793e7, #554ad7 500px)" class="s-call-to-action__overlay"></div> <div class="container"> <div class="s-call-to-action__wrapper"> <h3 class="s-call-to-action__title _medium _title _lighter">Need help with a quote for your treatment? Call us!</h3> <h2 class="s-call-to-action__phone _title _medium _lighter"><a href="tel:1300238648" class="s-call-to-action__phone-link"><svg focusable="false" version="1.1" width="0" height="0" class="svg-phone"><use xlink:href="#svg-phone"></use></svg>1300 238 648</a></h2> <h4 class="s-call-to-action__time _lighter">Monday - Friday 9am - 5pm AEST</h4> </div> </div> </section>',
            },
        ],
    },

    styleManager: {
        sectors: [{
            name: 'General',
            open: false,
            buildProps: ['float', 'display', 'position', 'top', 'right', 'left', 'bottom']
        }, {
            name: 'Dimension',
            open: false,
            buildProps: ['width', 'height', 'max-width', 'min-height', 'margin', 'padding'],
        }, {
            name: 'Typography',
            open: false,
            buildProps: ['font-family', 'font-size', 'font-weight', 'letter-spacing', 'color', 'line-height', 'text-align', 'text-shadow'],
            properties: [{
                property: 'text-align',
                list: [
                    {value: 'left', className: 'fa fa-align-left'},
                    {value: 'center', className: 'fa fa-align-center'},
                    {value: 'right', className: 'fa fa-align-right'},
                    {value: 'justify', className: 'fa fa-align-justify'}
                ],
            }]
        }, {
            name: 'Decorations',
            open: false,
            buildProps: ['border-radius-c', 'background-color', 'border-radius', 'border', 'box-shadow', 'background'],
        }, {
            name: 'Extra',
            open: false,
            buildProps: ['opacity', 'transition', 'perspective', 'transform'],
            properties: [{
                type: 'slider',
                property: 'opacity',
                defaults: 1,
                step: 0.01,
                max: 1,
                min: 0,
            }]
        }, {
            name: 'Flex',
            open: false,
            properties: [{
                name: 'Flex Container',
                property: 'display',
                type: 'select',
                defaults: 'block',
                list: [{
                    value: 'block',
                    name: 'Disable',
                }, {
                    value: 'flex',
                    name: 'Enable',
                }],
            }, {
                name: 'Flex Parent',
                property: 'label-parent-flex',
            }, {
                name: 'Direction',
                property: 'flex-direction',
                type: 'radio',
                defaults: 'row',
                list: [{
                    value: 'row',
                    name: 'Row',
                    className: 'icons-flex icon-dir-row',
                    title: 'Row',
                }, {
                    value: 'row-reverse',
                    name: 'Row reverse',
                    className: 'icons-flex icon-dir-row-rev',
                    title: 'Row reverse',
                }, {
                    value: 'column',
                    name: 'Column',
                    title: 'Column',
                    className: 'icons-flex icon-dir-col',
                }, {
                    value: 'column-reverse',
                    name: 'Column reverse',
                    title: 'Column reverse',
                    className: 'icons-flex icon-dir-col-rev',
                }],
            }, {
                name: 'Wrap',
                property: 'flex-wrap',
                type: 'radio',
                defaults: 'nowrap',
                list: [{
                    value: 'nowrap',
                    title: 'Single line',
                }, {
                    value: 'wrap',
                    title: 'Multiple lines',
                }, {
                    value: 'wrap-reverse',
                    title: 'Multiple lines reverse',
                }],
            }, {
                name: 'Justify',
                property: 'justify-content',
                type: 'radio',
                defaults: 'flex-start',
                list: [{
                    value: 'flex-start',
                    className: 'icons-flex icon-just-start',
                    title: 'Start',
                }, {
                    value: 'flex-end',
                    title: 'End',
                    className: 'icons-flex icon-just-end',
                }, {
                    value: 'space-between',
                    title: 'Space between',
                    className: 'icons-flex icon-just-sp-bet',
                }, {
                    value: 'space-around',
                    title: 'Space around',
                    className: 'icons-flex icon-just-sp-ar',
                }, {
                    value: 'center',
                    title: 'Center',
                    className: 'icons-flex icon-just-sp-cent',
                }],
            }, {
                name: 'Align',
                property: 'align-items',
                type: 'radio',
                defaults: 'center',
                list: [{
                    value: 'flex-start',
                    title: 'Start',
                    className: 'icons-flex icon-al-start',
                }, {
                    value: 'flex-end',
                    title: 'End',
                    className: 'icons-flex icon-al-end',
                }, {
                    value: 'stretch',
                    title: 'Stretch',
                    className: 'icons-flex icon-al-str',
                }, {
                    value: 'center',
                    title: 'Center',
                    className: 'icons-flex icon-al-center',
                }],
            }, {
                name: 'Flex Children',
                property: 'label-parent-flex',
            }, {
                name: 'Order',
                property: 'order',
                type: 'integer',
                defaults: 0,
                min: 0
            }, {
                name: 'Flex',
                property: 'flex',
                type: 'composite',
                properties: [{
                    name: 'Grow',
                    property: 'flex-grow',
                    type: 'integer',
                    defaults: 0,
                    min: 0
                }, {
                    name: 'Shrink',
                    property: 'flex-shrink',
                    type: 'integer',
                    defaults: 0,
                    min: 0
                }, {
                    name: 'Basis',
                    property: 'flex-basis',
                    type: 'integer',
                    units: ['px', '%', ''],
                    unit: '',
                    defaults: 'auto',
                }],
            }, {
                name: 'Align',
                property: 'align-self',
                type: 'radio',
                defaults: 'auto',
                list: [{
                    value: 'auto',
                    name: 'Auto',
                }, {
                    value: 'flex-start',
                    title: 'Start',
                    className: 'icons-flex icon-al-start',
                }, {
                    value: 'flex-end',
                    title: 'End',
                    className: 'icons-flex icon-al-end',
                }, {
                    value: 'stretch',
                    title: 'Stretch',
                    className: 'icons-flex icon-al-str',
                }, {
                    value: 'center',
                    title: 'Center',
                    className: 'icons-flex icon-al-center',
                }],
            }]
        }

        ],

    },

});


window.editor = editor;

var pnm = editor.Panels;
pnm.addButton('options', [{
    id: 'undo',
    className: 'fa fa-undo icon-undo',
    command: function (editor, sender) {
        sender.set('active', 0);
        editor.UndoManager.undo(1);
    },
    attributes: {title: 'Undo (CTRL/CMD + Z)'}
}, {
    id: 'redo',
    className: 'fa fa-repeat icon-redo',
    command: function (editor, sender) {
        sender.set('active', 0);
        editor.UndoManager.redo(1);
    },
    attributes: {title: 'Redo (CTRL/CMD + SHIFT + Z)'}
}, {
    id: 'get-html',
    className: 'fa fa-save icon-save',
    command: function (editor, sender) {

        var pathArray = window.location.pathname.split('/');
        var slug = pathArray[pathArray.length - 1];
        $.ajax({
            url: '/save',
            method: 'POST',
            data: {
                slug: slug,
                description: editor.getHtml(),
                css: editor.getCss()
            },
            success: function (response) {
                if (response.error == 0) {
                    location.href = '/home';
                } else {
                    alert("Error Occured");
                }
            }
        });
    },
    attributes: {title: 'Get HTML (CTRL/CMD + SHIFT + Z)'}
}, {
    id: 'clean-all',
    className: 'fa fa-trash icon-blank',
    command: function (editor, sender) {
        if (sender) sender.set('active', false);
        if (confirm('Are you sure to clean the canvas?')) {
            editor.DomComponents.clear();
            setTimeout(function () {
                localStorage.clear();
            }, 0);
        }
    },
    attributes: {title: 'Empty canvas'}
}]);

var bm = editor.BlockManager;
/*
 bm.add('link-block', {
 label: 'Link Block',
 attributes: {class:'fa fa-link'},
 category: 'Basic',
 content: {
 type:'link',
 editable: false,
 droppable: true,
 style:{
 display: 'inline-block',
 padding: '5px',
 'min-height': '50px',
 'min-width': '50px'
 }
 },
 });*/

var domc = editor.DomComponents;
var defaultType = domc.getType('default');
var defaultModel = defaultType.model;
var defaultView = defaultType.view;

/*
 domc.addType('default', {
 model: defaultModel.extend({
 defaults: Object.assign({}, defaultModel.prototype.defaults, {
 traits: [{
 name: 'title',
 label: 'Título',
 placeholder: 'Insira um texto aqui'
 }]
 }),
 }),
 });
 */


// Store and load events
editor.on('storage:load', function (e) {
    console.log('LOAD ', e);
})
editor.on('storage:store', function (e) {
    console.log('STORE ', e);
})

editor.on('styleManager:change:border-width', function (view) {
    var model = view.model;
    let targetValue = view.getTargetValue({ignoreDefault: 1});
    let computedValue = view.getComputedValue();
    let defaultValue = view.model.getDefaultValue();
    //console.log('Style of ', model.get('property'), 'Target: ', targetValue, 'Computed:', computedValue, 'Default:', defaultValue);
});

editor.render();
