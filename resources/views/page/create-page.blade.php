@extends('layouts.app')

@section('content')
    <div class="container">
    <form action="{{ route('page-store') }}" method="post">
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" name="title">
        </div>
        <div class="form-group">
            <label for="slug">Slug</label>
            <input type="text" name="slug" class="form-control">
        </div>

        <input type="submit" name="save-page" value="Save Page" class="btn btn-primary">
    </form>
    </div>
@endsection