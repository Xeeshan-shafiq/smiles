@extends('layouts.app')

@section('head')
    <style>
        {!! $page->css !!}
    </style>
@endsection

@section('content')
    {!!  html_entity_decode($page->description) !!}
    <footer class="s-footer">
        <div class="s-footer__top">
            <div class="container">
                <div class="s-footer__top-wrapper">
                    <div class="s-footer__nav">
                        <ul role="navigation" class="m-secondary">
                            <li class="m-secondary__item">
                                <h4 class="m-secondary__title">About</h4>
                                <ul class="m-secondary__sublist">
                                    <li class="m-secondary__subitem"><a href="#" class="m-secondary__sublink">about us</a>
                                    </li>
                                    <li class="m-secondary__subitem"><a href="#" class="m-secondary__sublink">careers</a>
                                    </li>
                                    <li class="m-secondary__subitem"><a href="#" class="m-secondary__sublink">blog</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="m-secondary__item">
                                <h4 class="m-secondary__title">Get in touch</h4>
                                <ul class="m-secondary__sublist">
                                    <li class="m-secondary__subitem"><a href="#" class="m-secondary__sublink">contact</a>
                                    </li>
                                    <li class="m-secondary__subitem"><a href="#" class="m-secondary__sublink">provider info</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="m-secondary__item">
                                <h4 class="m-secondary__title">Learn more</h4>
                                <ul class="m-secondary__sublist">
                                    <li class="m-secondary__subitem"><a href="#" class="m-secondary__sublink">smile.com.au pricing</a>
                                    </li>
                                    <li class="m-secondary__subitem"><a href="#" class="m-secondary__sublink">health fund partners</a>
                                    </li>
                                    <li class="m-secondary__subitem"><a href="#" class="m-secondary__sublink">discover savings</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="m-secondary__item">
                                <h4 class="m-secondary__title">Support</h4>
                                <ul class="m-secondary__sublist">
                                    <li class="m-secondary__subitem"><a href="#" class="m-secondary__sublink">faqs</a>
                                    </li>
                                    <li class="m-secondary__subitem"><a href="#" class="m-secondary__sublink">privacy</a>
                                    </li>
                                    <li class="m-secondary__subitem"><a href="#" class="m-secondary__sublink">terms</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="s-footer__contacts">
                        <div class="l-social">
                            <ul class="l-social__list">
                                <li class="l-social__item">
                                    <a href="#" class="l-social__link">
                                        <svg focusable="false" version="1.1" width="0" height="0" class="svg-facebook l-social__facebook">
                                            <use xlink:href="#svg-facebook"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li class="l-social__item">
                                    <a href="#" class="l-social__link">
                                        <svg focusable="false" version="1.1" width="0" height="0" class="svg-instagram l-social__instagram">
                                            <use xlink:href="#svg-instagram"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li class="l-social__item">
                                    <a href="#" class="l-social__link">
                                        <svg focusable="false" version="1.1" width="0" height="0" class="svg-twitter l-social__twitter">
                                            <use xlink:href="#svg-twitter"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li class="l-social__item">
                                    <a href="#" class="l-social__link">
                                        <svg focusable="false" version="1.1" width="0" height="0" class="svg-google-plus l-social__google-plus">
                                            <use xlink:href="#svg-google-plus"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li class="l-social__item">
                                    <a href="#" class="l-social__link">
                                        <svg focusable="false" version="1.1" width="0" height="0" class="svg-linkedin-transparent l-social__linkedin-transparent">
                                            <use xlink:href="#svg-linkedin-transparent"></use>
                                        </svg>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="s-footer__contacts-wrapper">
                            <div class="s-footer__contacts-phone">
                                <a href="tel:1300238648" class="s-footer__contacts-link">
                                    <svg focusable="false" version="1.1" width="0" height="0" class="svg-phone">
                                        <use xlink:href="#svg-phone"></use>
                                    </svg>1300 238 648</a>
                            </div>
                            <p class="s-footer__contacts-info">Monday - Friday 9am - 5pm AEST</p>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn _primary _bg _bordered">
                    <div class="btn__wrapper">
                        <div class="btn__icon">
                            <svg focusable="false" version="1.1" width="0" height="0" class="svg-practice">
                                <use xlink:href="#svg-practice"></use>
                            </svg>
                        </div>
                        <div class="btn__text">Discover the benefits to your practice</div>
                        <div class="btn__progress">
                            <div class="btn__progress-icon">
                                <svg focusable="false" version="1.1" width="0" height="0" class="svg-progress">
                                    <use xlink:href="#svg-progress"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                </button>
            </div>
        </div>
        <div class="s-footer__bottom">
            <div class="container">
                <div class="s-footer__bottom-wrapper">
                    <div class="s-footer__menu">
                        <div class="s-footer__menu-item _accordion">
                            <button type="button" data-accordion="{&quot;scrollTo&quot;: true, &quot;time&quot;: 300}" class="s-footer__menu-btn _accordion-btn js-toggleAccordion"><span>smile.com.au dentist locations</span>
                                <svg focusable="false" version="1.1" width="0" height="0" class="svg-arrow">
                                    <use xlink:href="#svg-arrow"></use>
                                </svg>
                            </button>
                            <div class="s-footer__menu-dropdown _accordion-dropdown">
                                <ul class="s-footer__menu-submenu">
                                    <li class="s-footer__menu-subitem"><a href="#" class="s-footer__menu-sublink">Adelaide</a>
                                    </li>
                                    <li class="s-footer__menu-subitem"><a href="#" class="s-footer__menu-sublink">Melbourne</a>
                                    </li>
                                    <li class="s-footer__menu-subitem"><a href="#" class="s-footer__menu-sublink">Brisbane</a>
                                    </li>
                                    <li class="s-footer__menu-subitem"><a href="#" class="s-footer__menu-sublink">Perth</a>
                                    </li>
                                    <li class="s-footer__menu-subitem"><a href="#" class="s-footer__menu-sublink">Canberra</a>
                                    </li>
                                    <li class="s-footer__menu-subitem"><a href="#" class="s-footer__menu-sublink">Sydney</a>
                                    </li>
                                    <li class="s-footer__menu-subitem"><a href="#" class="s-footer__menu-sublink">Darwin</a>
                                    </li>
                                    <li class="s-footer__menu-subitem"><a href="#" class="s-footer__menu-sublink">Sunshine Coast</a>
                                    </li>
                                    <li class="s-footer__menu-subitem"><a href="#" class="s-footer__menu-sublink">Gold Coast</a>
                                    </li>
                                    <li class="s-footer__menu-subitem"><a href="#" class="s-footer__menu-sublink">Wollongong</a>
                                    </li>
                                    <li class="s-footer__menu-subitem"><a href="#" class="s-footer__menu-sublink">Hobart</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="s-footer__menu-item _accordion">
                            <button type="button" data-accordion="{&quot;scrollTo&quot;: true, &quot;time&quot;: 300}" class="s-footer__menu-btn _accordion-btn js-toggleAccordion"><span>treatment information</span>
                                <svg focusable="false" version="1.1" width="0" height="0" class="svg-arrow">
                                    <use xlink:href="#svg-arrow"></use>
                                </svg>
                            </button>
                            <div class="s-footer__menu-dropdown _accordion-dropdown">
                                <ul class="s-footer__menu-submenu">
                                    <li class="s-footer__menu-subitem"><a href="#" class="s-footer__menu-sublink">Cosmetic</a>
                                    </li>
                                    <li class="s-footer__menu-subitem"><a href="#" class="s-footer__menu-sublink">Root Canal</a>
                                    </li>
                                    <li class="s-footer__menu-subitem"><a href="#" class="s-footer__menu-sublink">Dentistry</a>
                                    </li>
                                    <li class="s-footer__menu-subitem"><a href="#" class="s-footer__menu-sublink">Teeth Whitening</a>
                                    </li>
                                    <li class="s-footer__menu-subitem"><a href="#" class="s-footer__menu-sublink">Dental Braces</a>
                                    </li>
                                    <li class="s-footer__menu-subitem"><a href="#" class="s-footer__menu-sublink">Dentures</a>
                                    </li>
                                    <li class="s-footer__menu-subitem"><a href="#" class="s-footer__menu-sublink">Dental Bridges</a>
                                    </li>
                                    <li class="s-footer__menu-subitem"><a href="#" class="s-footer__menu-sublink">Endodontics</a>
                                    </li>
                                    <li class="s-footer__menu-subitem"><a href="#" class="s-footer__menu-sublink">Dental Crowns</a>
                                    </li>
                                    <li class="s-footer__menu-subitem"><a href="#" class="s-footer__menu-sublink">Gum Disease</a>
                                    </li>
                                    <li class="s-footer__menu-subitem"><a href="#" class="s-footer__menu-sublink">Dental Implants</a>
                                    </li>
                                    <li class="s-footer__menu-subitem"><a href="#" class="s-footer__menu-sublink">Wisdom Teeth</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="s-footer__copyright">© 2017 smile.com.au</div>
                </div>
            </div>
        </div>
    </footer>
@endsection
