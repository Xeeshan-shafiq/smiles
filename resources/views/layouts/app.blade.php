<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Smiles</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link href="{{ asset('bower_components/grapesjs/dist/css/grapes.min.css') }}" rel="stylesheet">
    @yield('head')
</head>
<body>
<div id="app">
    {{--  <nav class="navbar navbar-default navbar-static-top">
          <div class="container">
              <div class="navbar-header">

                  <!-- Collapsed Hamburger -->
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                      <span class="sr-only">Toggle Navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                  </button>

                  <!-- Branding Image -->
                  <a class="navbar-brand" href="{{ url('/') }}">
                      {{ config('app.name', 'Laravel') }}
                  </a>
              </div>

              <div class="collapse navbar-collapse" id="app-navbar-collapse">
                  <!-- Left Side Of Navbar -->
                  <ul class="nav navbar-nav">
                      &nbsp;
                  </ul>

                  <!-- Right Side Of Navbar -->
                  <ul class="nav navbar-nav navbar-right">
                      <!-- Authentication Links -->
                      @if (Auth::guest())
                          <li><a href="{{ route('login') }}">Login</a></li>
                          <li><a href="{{ route('register') }}">Register</a></li>
                      @else
                          <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                  {{ Auth::user()->name }} <span class="caret"></span>
                              </a>

                              <ul class="dropdown-menu" role="menu">
                                  <li>
                                      <a href="{{ route('logout') }}"
                                          onclick="event.preventDefault();
                                                   document.getElementById('logout-form').submit();">
                                          Logout
                                      </a>

                                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                          {{ csrf_field() }}
                                      </form>
                                  </li>
                              </ul>
                          </li>
                      @endif
                  </ul>
              </div>
          </div>
      </nav> --}}
</div>

<header class="s-header js-header">
    <div class="s-header__top">
        <div class="s-header__top-wrapper">
            <div class="s-header__logo">
                <a href="index.html" class="a-logo">
                    <img src="/images/logo.png" alt="">
                </a>
            </div>
            <div class="s-header__nav">
                <ul role="navigation" class="m-primary">
                    <li class="m-primary__item"><a href="#" class="m-primary__link"><span class="m-primary__link-text">what we do</span></a>
                    </li>
                    <li class="m-primary__item"><a href="#" class="m-primary__link"><span class="m-primary__link-text">how it works</span></a>
                    </li>
                    <li class="m-primary__item"><a href="#" class="m-primary__link"><span class="m-primary__link-text">pricing</span></a>
                    </li>
                    <li class="m-primary__item"><a href="#" class="m-primary__link"><span class="m-primary__link-text">contact</span></a>
                    </li>
                    <li class="m-primary__item"><a href="#" class="m-primary__link"><span class="m-primary__link-text">login</span></a>
                    </li>
                </ul>
            </div>
            <div class="s-header__actions">
                <a href="#" class="btn _lighter _bordered _secondary _find">
                    <div class="btn__wrapper">
                        <div class="btn__text">find a dentist</div>
                        <div class="btn__progress">
                            <div class="btn__progress-icon">
                                <svg focusable="false" version="1.1" width="0" height="0" class="svg-progress">
                                    <use xlink:href="#svg-progress"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="#" class="btn-link _lighter _login">
                    <div class="btn-link__wrapper">
                        <div class="btn-link__text">login</div>
                    </div>
                </a>
                <a href="#" class="btn _primary _join">
                    <div class="btn__wrapper">
                        <div class="btn__text">join</div>
                        <div class="btn__progress">
                            <div class="btn__progress-icon">
                                <svg focusable="false" version="1.1" width="0" height="0" class="svg-progress">
                                    <use xlink:href="#svg-progress"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                </a>
                <button type="button" class="btn-default _dots js-showDrawer">
                    <svg focusable="false" version="1.1" width="0" height="0" class="svg-dots">
                        <use xlink:href="#svg-dots"></use>
                    </svg>
                </button>
            </div>
        </div>
    </div>
</header>

<div class="content">
    @yield('content')
</div>


@yield('footer')
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
