@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Pages</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                            <a href="{{ route('page-create') }}" class="btn btn-primary">Create Page</a>
                        <table class="table table-stripped">
                            <thead>
                            <tr>
                                <th>Title</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($pages as $page)
                                <tr>
                                    <td><a href="{{ URL::to('page').'/'.$page->slug }}">{{ $page->title }}</a></td>
                                    <td><a href="{{ URL::to('editor').'/'.$page->slug }}">Edit Design</a></td>
                                    <td><a href="{{ URL::to('page').'/edit/'.$page->slug }}">Edit Settings</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
