@extends('layouts.editor')

@section('content')
    <div id="gjs" style="height: 700px">
        @if(isset($page))
            {!! $page->description !!}
            <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        @else
            <header class="header-banner">
                <div class="container-width">
                    <div class="logo-container">
                        <div class="logo">GrapesJS</div>
                    </div>
                    <nav class="menu">
                        <div class="menu-item">BUILDER</div>
                        <div class="menu-item">TEMPLATE</div>
                        <div class="menu-item">WEB</div>
                    </nav>
                    <div class="clearfix"></div>
                    <div class="lead-title">Build your templates without coding</div>
                    <div class="sub-lead-title">All text blocks could be edited easily with double clicking on it. You
                        can create new text blocks with the command from the left panel
                    </div>
                    <div class="lead-btn">Hover me</div>
                </div>
            </header>
            <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        @endif
    </div>

@endsection


@section('footer')
    <script>
        var files = [];
        files.push({type: 'image', src: 'http://placehold.it/350x250/78c5d6/fff/image1.jpg', height: 350, width: 250});
        files.push({type: 'image', src: 'http://placehold.it/350x250/459ba8/fff/image2.jpg', height: 350, width: 250});
        files.push({type: 'image', src: 'http://placehold.it/350x250/79c267/fff/image3.jpg', height: 350, width: 250});
        files.push({type: 'image', src: 'http://placehold.it/350x250/c5d647/fff/image4.jpg', height: 350, width: 250});
        files.push({type: 'image', src: 'http://placehold.it/350x250/f28c33/fff/image5.jpg', height: 350, width: 250});
        files.push({type: 'image', src: 'http://placehold.it/350x250/e868a2/fff/image6.jpg', height: 350, width: 250});
        files.push({type: 'image', src: 'http://placehold.it/350x250/cc4360/fff/image7.jpg', height: 350, width: 250});
                @foreach($images as $image)
        var temp = {
                src: "{{ asset('/app/public/uploads/'.$image->url) }}"
            };
        files.push(temp);
                @endforeach
        var css = "{!! $page->css !!}";
    </script>
@endsection