<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/editor', 'PageController@pageEditor')->name('page-editor');
Route::get('/editor/{slug}', 'PageController@editSinglePage')->name('single-page-editor');

Route::get('page/create','PageController@create')->name('page-create');
Route::post('page/store','PageController@store')->name('page-store');
Route::get('page/{slug}','PageController@page');

Route::post('image-upload','HomeController@imageUpload');

Route::post('save','PageController@savePage');
